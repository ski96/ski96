$(document).ready(function(){

  var containerScrll;
  $(".container").on("scroll", function(){
    var scrllTop = $(".container").scrollTop(),
        scrllLeft = $("body").scrollLeft();

    if(scrllTop>0){
      $(".wrapper .top").addClass("fix");
      if(scrllTop>=66){
        $(".wrapper .top").addClass("bg_white");
      }
    }else{
      $(".wrapper .top").removeClass("fix bg_white");
    }
    console.log(scrllLeft);
  });
  $(".container").on("scroll", function(){

  });
  $(".wrapper .top .nav li a").on("click", function(e){
    e.preventDefault();

    var container = $(".container"),
        $that = $(this),
        elId = $that.attr("href"),
        scrollTo = $(elId);
        // scrll = $el.position().top - 70;

  // alert(scrll)
  //
  //   $('.container').animate( { scrollTop: scrll }, 1000 );


  // Or you can animate the scrolling:
    container.animate({
      scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() - 90
    });
  });

  $(".call_right-box").on("click", function(){
    var $that = $("#"+$(this).data("elid"));

    $(".wrapper .describtion-block .block").removeClass("show");
    $(".wrapper").addClass("describtion-block_active");

    $that.addClass("show");
  });
  $(".wrapper .describtion-block .back-btn, .shadow").on("click", function(){
    $(".wrapper").removeClass("describtion-block_active");
  });

  $(".questionnaire .drop-list .option-list .option").on("click", function(){
    var $that = $(this),
        val = $that.html(),
        $parent = $that.parents(".option-list"),
        $select = $that.parents(".drop-list").children(".select");

    $parent.children(".option").removeClass("active");
    $that.addClass("active");
    $select.removeClass("no_select");
    $select.html(val);
    $parent.parent().children(".select").click();
  });
  $(".questionnaire .drop-list .drop-list_check").on("change", function(){
    $that = $(this),
    $select = $that.parents(".drop-list").children(".select");

    if($that.is(":checked")){
      $select.addClass("active")
    }else{
      $select.removeClass("active")
    }
  });
  $('html').click(function() {
      $(".questionnaire .drop-list .drop-list_check, .questionnaire .set-time input").prop("checked", false).change();
  });
  $(".questionnaire .drop-list, .questionnaire .set-time").click(function(event){
      event.stopPropagation();
  });

  $(".show-trigger").on("click", function(){
    var $el = $("#"+$(this).data("el"));

    $el.addClass("show");
  });

  $(".set-time").each(function(){
    var $that = $(this),
        $container = $('<div class="set-time_scroll-bar" />'),
        $topTime = $('<ul class="top-time" />'),
        topTimeContent = "",
        $line = $('<div class="line" />'),
        $lineSelect = $('<div class="line-select" />'),
        $start = $('<div class="start" />'),
        $finish = $('<div class="finish" />');

    for(var i=8;i<20;i++){
      var clss = (i==13 || i==14) ? "active" : "";

      topTimeContent += '<li data-hour="' + i + '" data-min="00">' + i + '</li>';
      if(i<19){
        topTimeContent += '<li class="' + clss + '" data-hour="' + i + '" data-min="30"></li>';
      }
    }
    $topTime.append(topTimeContent);

    $container.append($topTime, $finish, $start, $line, $lineSelect);
    $that.append($container);
  });

  function selectSet(l, r, time) {
    var $that = $(".set-time .set-time_scroll-bar .line-select"),
        $label = $that.parents(".set-time").children(".set-time_label"),
        $start = $label.children(".set-time_start"),
        $finish = $label.children(".set-time_finish"),
        $parent = $that.parents(".set-time").children(".set-time_scroll-bar").children(".top-time");

    var currentStart = ((l - 81) < 0) ? 0 : (((l - 81)/28)),
        currentFinish = ((r - 25)/28)+2;

    $parent.find("li").removeClass("active")
    $parent.find("li").eq(currentStart).addClass("active");
    $parent.find("li").eq(currentFinish).addClass("active");

    if(time=="start"){
      console.log($label.html())
      $start.html($parent.find("li").eq(currentStart).data("hour")+":"+$parent.find("li").eq(currentStart).data("min"));
    }else{
      console.log($label.html())
      $finish.html($parent.find("li").eq(currentFinish).data("hour")+":"+$parent.find("li").eq(currentFinish).data("min"));
    }

    $that.show();
    l -=92;
    $that.css({
      left: l,
      width:  r-l+28
    })


  }

  var leftStart = 333, leftFinish = 389;

  $(".set-time .set-time_scroll-bar .start").draggable({
    axis: "x",
    grid: [ 28, 0 ],
    containment: "parent",
    drag: function( event, ui ){
       ui.position.left = Math.min( leftStart, ui.position.left );
       $(".set-time .set-time_scroll-bar .line-select").hide();
    },
    stop: function( event, ui ){
      leftFinish = ui.offset.left - 580;
      selectSet(leftFinish, leftStart, "start");
    }
  });
  $(".set-time .set-time_scroll-bar .finish").draggable({
    axis: "x",
    grid: [ 28, 0 ],
    containment: "parent",
    drag: function( event, ui ){
       ui.position.left = Math.max( leftFinish, ui.position.left );
       $(".set-time .set-time_scroll-bar .line-select").hide();
    },
    stop: function( event, ui ){
      leftStart = ui.offset.left - 692;
      selectSet(leftFinish, leftStart, "finish");
    }
  });

  $(".gallery-block .item").on("click", function(){
    $(".wrapper").addClass("gallery_active");
  });

  $("html").on("click", ".wrapper.gallery_active .shadow", function(){
    $(".wrapper").removeClass("gallery_active");

    $("body").scrollTop(containerScrll);
  });

  var b = bowser.name;
  $('body').addClass(b);

  setInterval(function(){
    $("canvas").each(function(){
      $(this).css("top", "-10000%");
    });
  }, 10)
});
